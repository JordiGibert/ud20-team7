package ex7;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ex7 extends JFrame {

	private JPanel contentPane;
	private JTextField textConvertir;
	private JTextField textResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ex7 frame = new ex7();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ex7() {
		setTitle("Calculadora Cambio de Monedas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel convertir = new JLabel("Cantiad a Convertir");
		convertir.setFont(new Font("Tahoma", Font.PLAIN, 13));
		convertir.setBounds(10, 32, 117, 16);
		contentPane.add(convertir);
		
		textConvertir = new JTextField();
		textConvertir.setBounds(125, 31, 86, 20);
		contentPane.add(textConvertir);
		textConvertir.setColumns(10);
		
		JLabel resultado = new JLabel("Resultado");
		resultado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		resultado.setBounds(227, 34, 73, 14);
		contentPane.add(resultado);
		
		textResultado = new JTextField();
		textResultado.setBounds(292, 31, 86, 20);
		contentPane.add(textResultado);
		textResultado.setColumns(10);
		
		JButton btnConvertir = new JButton("Euro a Pesetas");
		btnConvertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				double Moneda = Double.parseDouble(textConvertir.getText());
				double Resultado;
				
				Resultado = 166.386 / Moneda ;
						
				
				 JOptionPane.showInputDialog( Moneda + " En Euros : " + Resultado);
				 
				 //textResultado.append( Moneda + " En Euros : " + Resultado);
			}
		});
		btnConvertir.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnConvertir.setBounds(70, 76, 141, 20);
		contentPane.add(btnConvertir);
		
		JButton btnCambiar = new JButton("Cambiar");
		btnCambiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				double Moneda = Double.parseDouble(textConvertir.getText());
				double Resultado;
				
				Resultado =  Moneda / 166.386;
				 JOptionPane.showInputDialog( Moneda + " En Pesetas es : " + Resultado);
				 
				//textResultado.append( Moneda + " En Pesetas: " + Resultado);
				
			}
		});
		btnCambiar.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnCambiar.setBounds(289, 75, 89, 23);
		contentPane.add(btnCambiar);
	}
}
