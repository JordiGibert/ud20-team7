package ex3;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ex2.ex2Visible;

public class ex3Visible extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ex3Visible frame = new ex3Visible();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private static int clic1 = 0;
	private static int clic2 = 0;
	/**
	 * Create the frame.
	 */
	public ex3Visible() {
		setTitle("Exercici 3");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl = new JLabel("No has pulsado ningun boton");
		lbl.setHorizontalAlignment(SwingConstants.CENTER);
		lbl.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbl.setBounds(37, 11, 315, 44);
		contentPane.add(lbl);
		
		JButton btn1 = new JButton("Boton 1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clic1++;
				lbl.setText("Boton 1: "+clic1+" veces Boton 2: "+clic2+" veces");
			}
		});
		btn1.setBounds(37, 66, 116, 44);
		contentPane.add(btn1);
		
		JButton btn2 = new JButton("Boton 2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clic2++;
				lbl.setText("Boton 1: "+clic1+" veces Boton 2: "+clic2+" veces");
				
			}
		});
		btn2.setBounds(180, 66, 116, 44);
		contentPane.add(btn2);
	}
}
