package ex9;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import javax.swing.SwingConstants;

public class Ex9Visible extends JFrame {

	
	private JPanel contentPane;	
	private JToggleButton tglbtn1;
	private JToggleButton tglbtn6;
	private JToggleButton tglbtn11;
	private JToggleButton tglbtn2;
	private JToggleButton tglbtn3;
	private JToggleButton tglbtn4;
	private JToggleButton tglbtn5;
	private JToggleButton tglbtn7;
	private JToggleButton tglbtn8;
	private JToggleButton tglbtn9;
	private JToggleButton tglbtn10;
	private JToggleButton tglbtn12;
	private JToggleButton tglbtn13;
	private JToggleButton tglbtn14;
	private JToggleButton tglbtn15;
	private JToggleButton tglbtn16;
	private JToggleButton tglbtn17;
	private JToggleButton tglbtn18;
	private boolean girarCarta = false;
	private boolean segundaCara = false;
	private ImageIcon cara;
	private ImageIcon cruz;
	private JToggleButton[] precionabotton = new JToggleButton[2];
	private int intentos = 0;
	private int asiertos = 0;
	private JLabel labelIntentos;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex9Visible frame = new Ex9Visible();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	// Genera un array de 18 números aleatorios desde 1 al 9
	private int[] numerarCartas() {
		int[] num = new int[18];
		int coun = 0;
		
		 while(coun < 18) {
	            Random r = new Random();
	            int na = r.nextInt(9) + 1;
	            int nvr = 0;
	            
	            for (int i = 0; i < 18; i++) {
	                if(num[i] == na) {
	                    nvr++;
	                }
	            }
	            if(nvr < 2) {
	                num[coun] = na;
	                coun++;
	            }//fin
	            
	        }
		 return num;		
	}
	
	// Recive el Toggle
	public void TglbtnHabilitar(JToggleButton tglbtn) {	
		tglbtn.setEnabled(false);
		
		if(!girarCarta) {			
			cara = (ImageIcon) tglbtn.getDisabledIcon();
			precionabotton[0] = tglbtn;
			girarCarta = true;
			segundaCara = false;
		
		}
		else {
			cruz = (ImageIcon) tglbtn.getDisabledIcon();
			precionabotton[1] = tglbtn;
			segundaCara = true;
			
		}		
	}
	
	// Compara si son las dos caras iguales
	public void comparar() {
		if(girarCarta && segundaCara) {			
			if(cara.getDescription().compareTo(cruz.getDescription()) != 0) {
				precionabotton[0].setEnabled(true);
				precionabotton[1].setEnabled(true);
				intentos++;
				labelIntentos.setText(String.valueOf(intentos));
			}else {
				asiertos++;
			}
			girarCarta = false;
		}
	
		if(asiertos == 9) {
			JOptionPane.showMessageDialog(null, "Enhorabuena!!", "Fin del juego", JOptionPane.CLOSED_OPTION);
			System.exit(0);
		}
		

	}
	
	public Ex9Visible() {		
		int[] numeroAleatorios = numerarCartas();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 851, 758);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 160, 122));
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		tglbtn1 = new JToggleButton("");
		tglbtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn1.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[0]+".png")));
		tglbtn1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		
		tglbtn1.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn1.setBounds(10, 11, 128, 205);
		tglbtn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn1);
			}
		});	
		
		contentPane.setLayout(null);
		contentPane.add(tglbtn1);
		
		tglbtn6 = new JToggleButton("");
		tglbtn6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn6.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[1]+".png")));
		tglbtn6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn6.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn6.setBounds(10, 220, 128, 205);
		tglbtn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn6);
			}
		});
		contentPane.add(tglbtn6);
		
		tglbtn11 = new JToggleButton("");
		tglbtn11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn11.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[2]+".png")));
		tglbtn11.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn11.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn11.setBounds(10, 429, 128, 205);
		tglbtn11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn11);
			}
		});
		contentPane.add(tglbtn11);
		
		tglbtn2 = new JToggleButton("");
		tglbtn2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn2.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[3]+".png")));
		tglbtn2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn2.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn2.setBounds(148, 11, 128, 205);
		tglbtn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn2);
			}
		});
		contentPane.add(tglbtn2);
		
		tglbtn3 = new JToggleButton("");
		tglbtn3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn3.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[4]+".png")));
		tglbtn3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn3.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn3.setBounds(286, 11, 128, 205);
		tglbtn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn3);
			}
		});
		contentPane.add(tglbtn3);
		
		tglbtn4 = new JToggleButton("");
		tglbtn4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn4.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[5]+".png")));
		tglbtn4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn4.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn4.setBounds(424, 11, 128, 205);
		tglbtn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn4);
			}
		});
		contentPane.add(tglbtn4);
		
		tglbtn5 = new JToggleButton("");
		tglbtn5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn5.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[6]+".png")));
		tglbtn5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn5.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn5.setBounds(562, 11, 128, 205);
		tglbtn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn5);
			}
		});
		contentPane.add(tglbtn5);
		
		tglbtn7 = new JToggleButton("");
		tglbtn7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn7.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[7]+".png")));
		tglbtn7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn7.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn7.setBounds(148, 220, 128, 205);
		tglbtn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn7);
			}
		});
		contentPane.add(tglbtn7);
		
		tglbtn8 = new JToggleButton("");
		tglbtn8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn8.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[8]+".png")));
		tglbtn8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn8.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn8.setBounds(286, 220, 128, 205);
		tglbtn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn8);
			}
		});
		contentPane.add(tglbtn8);
		
		tglbtn9 = new JToggleButton("");
		tglbtn9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn9.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[9]+".png")));
		tglbtn9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn9.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn9.setBounds(424, 220, 128, 205);
		tglbtn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn9);
			}
		});
		contentPane.add(tglbtn9);
		
		tglbtn10 = new JToggleButton("");
		tglbtn10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn10.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[10]+".png")));
		tglbtn10.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn10.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn10.setBounds(562, 220, 128, 205);
		tglbtn10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn10);
			}
		});
		contentPane.add(tglbtn10);
		
		tglbtn12 = new JToggleButton("");
		tglbtn12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn12.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[11]+".png")));
		tglbtn12.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn12.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn12.setBounds(148, 429, 128, 205);
		tglbtn12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn12);
			}
		});
		contentPane.add(tglbtn12);
		
		tglbtn13 = new JToggleButton("");
		tglbtn13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn13.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[12]+".png")));
		tglbtn13.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn13.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn13.setBounds(286, 429, 128, 205);
		tglbtn13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn13);
			}
		});
		contentPane.add(tglbtn13);
		
		tglbtn14 = new JToggleButton("");
		tglbtn14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn14.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[13]+".png")));
		tglbtn14.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn14.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn14.setBounds(424, 429, 128, 205);
		tglbtn14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn14);
			}
		});
		contentPane.add(tglbtn14);
		
		tglbtn15 = new JToggleButton("");
		tglbtn15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn15.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[14]+".png")));
		tglbtn15.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn15.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn15.setBounds(562, 429, 128, 205);
		tglbtn15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn15);
			}
		});
		contentPane.add(tglbtn15);		
		
		tglbtn16 = new JToggleButton("");
		tglbtn16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn16.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[15]+".png")));
		tglbtn16.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn16.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn16.setBounds(697, 11, 128, 205);
		tglbtn16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn16);
			}
		});
		contentPane.add(tglbtn16);
		
		tglbtn17 = new JToggleButton("");
		tglbtn17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn17.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[16]+".png")));
		tglbtn17.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn17.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn17.setBounds(697, 220, 128, 205);
		tglbtn17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn17);
			}
		});
		contentPane.add(tglbtn17);
		
		tglbtn18 = new JToggleButton("");
		tglbtn18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ex9/imagenes/0.png")));
		tglbtn18.setDisabledIcon(new ImageIcon(getClass().getResource("/ex9/imagenes/"+numeroAleatorios[17]+".png")));
		tglbtn18.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				comparar();
			}
		});
		tglbtn18.setFont(new Font("Tahoma", Font.BOLD, 45));
		tglbtn18.setBounds(697, 429, 128, 205);
		tglbtn18.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TglbtnHabilitar(tglbtn18);
			}
		});
		contentPane.add(tglbtn18);
		
		JLabel lblNewLabel = new JLabel("Intentos:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 40));
		lblNewLabel.setBounds(520, 659, 200, 49);
		contentPane.add(lblNewLabel);
		
		labelIntentos = new JLabel("0");
		labelIntentos.setFont(new Font("Tahoma", Font.BOLD, 40));
		labelIntentos.setBounds(730, 659, 75, 49);
		contentPane.add(labelIntentos);
		
		
	}
}
