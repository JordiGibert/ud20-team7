package ex8;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.SystemColor;

public class ex8 extends JFrame {

	private JPanel contentPane;
	private JTextField textConvertir;
	private JTextField textResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ex8 frame = new ex8();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ex8() {
		setTitle("Calculadora Cambio de Monedas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel Convertir = new JLabel("Cantidad a convertir");
		Convertir.setFont(new Font("Tahoma", Font.PLAIN, 13));
		Convertir.setBounds(10, 37, 131, 16);
		contentPane.add(Convertir);
		
		textConvertir = new JTextField();
		textConvertir.setBounds(136, 36, 86, 20);
		contentPane.add(textConvertir);
		textConvertir.setColumns(10);
		
		JLabel Resultado = new JLabel("Resultado");
		Resultado.setFont(new Font("Tahoma", Font.PLAIN, 13));
		Resultado.setBounds(232, 39, 58, 16);
		contentPane.add(Resultado);
		
		textResultado = new JTextField();
		textResultado.setBounds(302, 36, 86, 20);
		contentPane.add(textResultado);
		textResultado.setColumns(10);
		
		JButton btnConvertir = new JButton("Pesetas a Euros");
		btnConvertir.setBackground(Color.BLUE);
		btnConvertir.setForeground(Color.BLACK);
		btnConvertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//con esto recogemos los resultados que nos escriben en la parte grafica y se lo asignamos a esta variable
				double Moneda = Double.parseDouble(textConvertir.getText());
				double Resultado;
				
				Resultado = 166.386 / Moneda ;
						
				//textResultado.setText(Resultado);
				 JOptionPane.showInputDialog( Moneda + " En Euros : " + Resultado);
				 
				 //textResultado.append( Moneda + " En Euros : " + Resultado);
			}
		});
		btnConvertir.setBounds(10, 75, 166, 23);
		contentPane.add(btnConvertir);
		
		JButton btnCambiar = new JButton("Cambiar");
		btnCambiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				double Moneda = Double.parseDouble(textConvertir.getText());
				String Final1 = String.valueOf(Moneda);
				double Resultado = 0;
				String Final = String.valueOf(Resultado);
				
				Resultado =  Moneda / 166.386;
				 JOptionPane.showInputDialog( Moneda + " En Pesetas es : " + Resultado);
				 
			//textResultado.append( Moneda + " En Pesetas: " + Resultado);
			}
		});
		btnCambiar.setBounds(197, 75, 89, 23);
		contentPane.add(btnCambiar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Para poder borrar, enviamos el mensaje vacio asi el boton borrar eliminara la casilla , la vaciara
				textConvertir.setText("");
				textResultado.setText("");
				//y con esto hacemos que una vez que apretamos el boton borrar se ponga el cursor en la casilla que hemos marcado
				textConvertir.grabFocus();
				
			}
		});
		btnBorrar.setForeground(Color.BLACK);
		btnBorrar.setBounds(302, 75, 89, 23);
		contentPane.add(btnBorrar);
	}

}
