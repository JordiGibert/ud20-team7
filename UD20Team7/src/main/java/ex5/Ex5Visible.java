package ex5;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.TextArea;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;

public class Ex5Visible extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex5Visible frame = new Ex5Visible();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	TextArea textArea = new TextArea();
	String text = "";
	public Ex5Visible() {
		
		
		setTitle("Exercici 5");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 615, 364);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		textArea.setBounds(10, 56, 579, 259);
		contentPane.add(textArea);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				text="";
				textArea.setText(text);
			}
		});
		btnLimpiar.setBounds(226, 11, 104, 39);
		contentPane.add(btnLimpiar);
		
		textArea.addMouseListener(sc);
	
	}
	
	MouseListener sc = new MouseListener() {
		@Override
		public void mouseClicked(MouseEvent e) {
			text=text+"Raton clikeado\n";
			textArea.setText(text);
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			text=text+"Boton de raton presso\n";
			textArea.setText(text);
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			text=text+"Boton de raton suelto\n";
			textArea.setText(text);
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			text=text+"Raton dentro de ventana\n";
			textArea.setText(text);
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			text=text+"Raton fuera de ventana\n";
			textArea.setText(text);
			
		}

	};
}
