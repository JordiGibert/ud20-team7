package ex6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ex6 extends JFrame {

	private JPanel contentPane;
	private  JTextField textAltrua;
	private JTextField textPeso;
	private JTextField textIMC;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ex6 frame = new ex6();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ex6() {
		setTitle("Indice de Masa Corporal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel Altura = new JLabel("Altura (metros)");
		Altura.setFont(new Font("Tahoma", Font.PLAIN, 13));
		Altura.setBounds(23, 33, 101, 14);
		contentPane.add(Altura);
		
		textAltrua = new JTextField();
		textAltrua.setBounds(120, 31, 86, 20);
		contentPane.add(textAltrua);
		textAltrua.setColumns(10);
		
		textPeso = new JTextField();
		textPeso.setBounds(294, 31, 86, 20);
		contentPane.add(textPeso);
		textPeso.setColumns(10);
		
		JLabel Peso = new JLabel("Peso (kg)");
		Peso.setFont(new Font("Tahoma", Font.PLAIN, 13));
		Peso.setBounds(228, 33, 64, 14);
		contentPane.add(Peso);
		
		JButton btnIMC = new JButton("Calcular IMC");
		btnIMC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				double Altura= Double.parseDouble(textAltrua.getText());
				double Peso= Double.parseDouble(textPeso.getText());
				double IMC;
				
				IMC = Peso /(Altura * Altura);
			
				  //textIMC.append("El IMC es + " IMC);
				
			
				
			}
		});
		btnIMC.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnIMC.setBounds(105, 76, 101, 30);
		contentPane.add(btnIMC);
		
		JLabel lblNewLabel = new JLabel("IMC");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(228, 80, 58, 21);
		contentPane.add(lblNewLabel);
		
		textIMC = new JTextField();
		textIMC.setBounds(294, 82, 86, 20);
		contentPane.add(textIMC);
		textIMC.setColumns(10);
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
