package ex4;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import java.awt.TextArea;

public class Ex4Visible extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex4Visible frame = new Ex4Visible();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	TextArea textArea = new TextArea();
	String text = "";
	public Ex4Visible() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				text=text+"Ventana activada\n";
				textArea.setText(text);
			}

			@Override
			public void windowOpened(WindowEvent e) {
				text=text+"Ventana abierta\n";
				textArea.setText(text);
				
			}

			@Override
			public void windowClosing(WindowEvent e) {
				text=text+"Cerrando ventana\n";
				textArea.setText(text);
				
			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				text=text+"Ventana cerradaa\n";
				textArea.setText(text);
				
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				text=text+"Ventana minimizada\n";
				textArea.setText(text);
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				text=text+"Ventana maximizada\n";
				textArea.setText(text);
				
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				text=text+"Ventana desactivada\n";
				textArea.setText(text);
				
			}
		});
		
		
		
		setTitle("Exercici 4");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Eventos");
		lblNewLabel.setBounds(10, 115, 46, 14);
		contentPane.add(lblNewLabel);
		
		
		textArea.setBounds(62, 10, 362, 241);
		contentPane.add(textArea);
		
		contentPane.addMouseListener(sc);
	
	}
	
	MouseListener sc = new MouseListener() {
		@Override
		public void mouseClicked(MouseEvent e) {
			text=text+"Click en ventana\n";
			textArea.setText(text);
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

	};
}

